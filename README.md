# Free IPA
Стенд разворачивается автоматически включает в себя 2 машины :

1. server: 192.168.10.10
2. client: 192.168.10.20
 
Provision поднимает ipa server и ipa client, и добавляет client в домен.

Конфигурация обеих машин
 
1. Переключить SE Linux в Permissive
2. Обновить все пакеты yum update -y
3. Влкючить и enablie firewalld (опционально)
4. Открыть порты в firewalld  tcp :53, 80, 88, 389, 443, 464, 636
5. Открыть порты в firewalld  udp :53, 88, 123, 464
6. Перезапустить firewalld


Конфигурация server:
1. Установка ipa-server ipa-server-dns  `yum install  ipa-server ipa-server-dns -y`
2. Установка hostname `hostnamectl set-hostname server.otus.lan`
3. Запуск конфигурирования `freeipa ipa-server-install -U -r OTUS.LAN --setup-dns --forwarder=10.0.2.3 -p 12345678 -a 87654321 --debug`
directory manager - 12345678, admin 87654321, об остальных опциях - man ipa-server-install

Конфигурация client
1. Установка ipa-client `yum install ipa-client -y`
2. останавливаем networkManager, чтобы сделать /etc/resolf.conf статичным `systemctl stop NetworkManager`
3. заменяем /etc/resolv.conf  на заранее подготовленный
4. Конфигурируем ipa client   `ipa-client-install -U -p admin -w 87654321 --mkhomedir`


Для проверки того, что всё получилось и клиент попал в домен -
1. С машины server нужно выполнить `kinit admin`
2. и следом команду `ipa host-show ` - увидим клиента в домене.